import { Counter } from './counter';

 describe('Counter', () => {
  const counter = new Counter();
   test('get count',() => {
      expect(counter.getValue()).toBe(0);
   });
  test('increment',() => {
      expect(counter.inc().getValue()).toBe(1);
   });
   test('decrement',() => {
      expect(counter.dec().getValue()).toBe(-1);
   });

});

 
