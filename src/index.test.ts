import { is, List } from "immutable";
import { isPrime, primesList} from './functions';

describe('primes', () => {
  test('throws range exception if input is less than 2',() => {
    expect(() => {isPrime(1)}).toThrow();
  });
  test('throws range exception if input is less than 2 for primelist',() => {
    expect(() => {primesList(1)}).toThrow();
  });
  test('primeslist of 2 is', () => {
   expect(is(primesList(2),List([2]))).toBe(true)
  });
  test('primeslist of first 100 numbers', () => {
    expect(is(primesList(100),List([2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97]))).toBe(true)
   });
});
  



