import { Range } from 'immutable'

export const factorial = (n: number): number => {
   if(n<0) { throw new RangeError("n should be positive number");}
   return Range(1,n+1).reduce((x,y) => x * y,1);
}

export const ncr = (n: number,r: number) => {
  if(n<r) {throw new RangeError("n should be greater than or equal to r");}
  if(r === 0 || n === r) {return 1;}
  return factorial(n) /(factorial(r) * (factorial(n-r))) ;
}

export const pascalLine = (line: number) => {
  if(line<0) { throw new RangeError("line number should be positive")}
  return Range(0,line+1).map(x => ncr(line,x))
}

export const pascalTriangle = (n: number) => {
  if(n<0) { throw new RangeError("input should be positive")}
  return Range(1,n+1).map(x => pascalLine(x-1))
 } 