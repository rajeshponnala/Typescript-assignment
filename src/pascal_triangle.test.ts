import { factorial, ncr, pascalLine, pascalTriangle } from './pascal_triangle';
import { List } from 'immutable';

describe('Factorial',() => {
  test('factorial throws range error if input is negative number', () => {
       expect(() => { factorial(-1)}).toThrow();
    }); 
  test('factorial of 0 should be 1', () => {
     expect(factorial(0)).toBe(1);
  });
  test('factorial of 1 should be 1', () => {
    expect(factorial(1)).toBe(1);
  });
  test('factorial of 10 should be 3628800', () => {
    expect(factorial(10)).toBe(3628800);
  });
});

describe('NCR',() => {
    test('throws error if n less than r in ncr', () => {
    expect(() => { ncr(10,11)}).toThrow();
  });
  test('ncr should be 1 if r is 0', () => {
    expect(ncr(10,0)).toBe(1);
  });
  test('ncr should be 1 if n and r both are same', () => {
    expect(ncr(10,10)).toBe(1);
  });
   test('ncr of 5, 4 should be 5', () => {
    expect(ncr(5,4)).toBe(5);
  });
  test('ncr of 5, 4 should be 5', () => {
    expect(ncr(5,4)).toBe(5);
  });
});

 describe('Pascal Line', () => {
   test('negative input to pascalLine throws error', () => {
    expect( () => {pascalLine(-1)}).toThrow();
  });
  test('0 to pascalLine should be List of [1]', () => {
    expect(pascalLine(0).toList()).toEqual(List([1]));
  });
   test('3 to pascalLine should be List of [1 2 1]', () => {
    expect(pascalLine(3).toList()).toEqual(List([1,3,3,1]));
  });
   test('6 to pascalLine should be List of [1,6,15,20,15,6,1]', () => {
    expect(pascalLine(6).toList()).toEqual(List([1,6,15,20,15,6,1]));
   });
 })

describe('Pascal triangle', () => {
  test('negative number to pascalTriangle throws error', () => {
    expect( () => { pascalTriangle(-7)}).toThrow();
  });
   test('0 line of pascalTriangle should be List of []', () => {
    expect(pascalTriangle(0).toList()).toEqual(List([]));
  });
   test('1 line of pascalTriangle should be List([List([1]))', () => {
    console.log(List(pascalTriangle(1).map(x => x.toList())))
    expect(List(pascalTriangle(1).map(x => x.toList()))).toEqual(List([List([1])]));
  });
   test('3 line of pascalTriangle should be List([ List([1]) , List([1,1]) , List([1,2,1])])', () => {
    expect(List(pascalTriangle(3).map(x => x.toList()))).toEqual(List([ List([1]) , List([1,1]) , List([1,2,1])]));
  });
   test('6 line of pascalTriangle should be List([ List([1]) , List([1,1]) , List([1,2,1]) , List([1,3,3,1]),List([1,4,6,4,1]) , List([1,5,10,10,5,1])]', () => {
    expect(List(pascalTriangle(6).map(x => x.toList()))).toEqual(List([ List([1]) , List([1,1]) , List([1,2,1]) , List([1,3,3,1]),List([1,4,6,4,1]) , List([1,5,10,10,5,1])]));
  });
 });


 



  







