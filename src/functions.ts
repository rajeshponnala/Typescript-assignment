import { Range } from "immutable";

export const isPrime = (n: number) => {
  if(n < 2) { throw new RangeError("number should be greater than 1")}  
  return !Range(2,n).some(x => n % x === 0);
}

export const primesList = (n: number) => {
   if(n < 2) { throw new RangeError("input should be greater than 1")}
   return Range(2,n+1).filter(isPrime);  
}



