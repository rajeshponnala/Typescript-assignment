export class Counter {
  count: number
  constructor(count:number = 0) {
     this.count = count;
  }
   inc = () => new Counter(this.count+1) 
   dec = () => new Counter(this.count-1)
   getValue = (): number => this.count
  } 